<?php
////////////////////////////////////////////////////////////////////////////
//                        Parameters
////////////////////////////////////////////////////////////////////////////

/// <summary>
///     directory to read
/// </summary>
$directory = 'FichiersCSV';

/// <summary>
///     number max in files name to rename
///     case up to 10 not managed
/// </summary>
$numericIndexMax = 10;

/// <summary>
///     connection object
/// </summary>
$DBConnection = null;

/// <summary>
///     sql settings initialisation
/// </summary>
$column = '';
$table = '';
$parameter = '';
$value = '';
$sqlRequests = [];

////////////////////////////////////////////////////////////////////////////
//                        Instantiation of database connection
////////////////////////////////////////////////////////////////////////////

/// <summary>
///     database connection object
/// </summary>
try{
    $DBConnection = new PDO('mysql:host=######;dbname=######','######','######');
    $DBConnection -> EXEC ('SET CHARACTER SET UTF8');
    $DBConnection -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch (Exception $ex){
    echo "Connection failed";
    throw $ex;
    var_dump($ex);
}

////////////////////////////////////////////////////////////////////////////
//                        Start of script
////////////////////////////////////////////////////////////////////////////

/// <summary>
///     kill script if non-valid directory
/// </summary>
/// <parameters>
///     $directory : the directory to check
/// </parameters>
if(!is_dir($directory)){
    die('Non-valid directory');
}

/// <summary>
///     read files name in directory and made an array with names
/// </summary>
/// <parameters>
///     $directory : the directory to scan
/// </parameters>
$files = scandir($directory);
$arrayCsvFilesNames = FilterCsvFiles($files);

/// <summary>
///     kill script if no csv file in directory
/// </summary>
/// <parameters>
///     $arrayCsvFilesNamess : array with csv files names
/// </parameters>
if(sizeof($arrayCsvFilesNames)==0){
    die('No csv file in directory');
}

/// <summary>
///     order files name in array and delete index with no value
/// </summary>
sort($arrayCsvFilesNames);

/// <summary>
///     rename files to order them in accordance with source excel file
/// </summary>
/// <parameters>
///     $arrayCsvFilesNames : array with the files names to proceed
/// </parameters>
foreach($arrayCsvFilesNames as &$csvFileName){
    
    $oldFileName = $csvFileName;

    /// <summary>
    ///     explodes the csv file name and extracts the first character
    /// </summary>
    $arrayFileName = explode('-', $csvFileName);
    $numericIndex = substr($arrayFileName[0],1);

    /// <summary>
    ///     rename file with numeric index less than numericIndexMax
    /// </summary>
    if($arrayFileName[0]<$numericIndexMax && $numericIndex == 0){
        
        /// <summary>
        ///     add a 0 to the file name when begining under 10
        ///     1 -> 01
        /// </summary>
        $arrayFileName[0]='0'.$arrayFileName[0];
        $newFileName = $arrayFileName[0]."-".$arrayFileName[1];
        $csvFileName = $newFileName;

        rename($directory."/".$oldFileName, $directory."/".$newFileName);
    }
}

sort($arrayCsvFilesNames);

/// <summary>
///     open and read csv files
/// </summary>
/// <parameters>
///     $arrayVscFilesName : array with the names of the csv files ordered according to the excel file
/// </parameters>
foreach($arrayCsvFilesNames as $fileToLoad){
    /// <summary>
    ///     complete csv file name with directory
    /// </summary>
    $fileToLoad = $directory."/".$fileToLoad;
    $arrayDatasLoaded=[];

    /// log
    echo "<p><b>opening file : ".$fileToLoad."</b></p>";

    $fileLoaded = fopen($fileToLoad, "r");
    
    /// <summary>
    ///     isolate each value of a csv line
    ///     push it in an array
    /// </summary>
    while ($datasLoaded = fgetcsv($fileLoaded, null, ",")) {
        $datasLoaded = str_replace(";","",$datasLoaded);
        array_push($arrayDatasLoaded, $datasLoaded);
    };

////////////////////////////////////////////////////////////////////////////
//                        Themes
////////////////////////////////////////////////////////////////////////////

    /// <summary>
    ///     define sql settings for theme
    ///     clean value variable
    /// </summary>
    $value = CleanString($arrayDatasLoaded[0][0]);
    $sqlRequests = [
        'selectRequest' => "SELECT `idTheme` FROM `themes` WHERE `theme` LIKE '$value';",
        'insertRequest' => "INSERT INTO `themes` (`theme`) VALUES ('$value');"
    ];

    $idTheme = ReturnId('idTheme', $sqlRequests, $DBConnection);

////////////////////////////////////////////////////////////////////////////
//                        Contextes
////////////////////////////////////////////////////////////////////////////

    /// <summary>
    ///     define sql settings for contexte
    ///     clean value variable
    /// </summary>
    $value = CleanString($arrayDatasLoaded[2][0]);
    $sqlRequests = [
        'selectRequest' => "SELECT `idContexte` FROM `contextes` WHERE `contexte` LIKE '$value';",
        'insertRequest' => "INSERT INTO `contextes` (`contexte`, `idTheme`) VALUES ('$value', '$idTheme');"
    ];

    $idContexte = ReturnId('idContexte', $sqlRequests, $DBConnection);

////////////////////////////////////////////////////////////////////////////
//                        Language
////////////////////////////////////////////////////////////////////////////

    /// <summary>
    ///     define sql settings for origine language (french)
    ///     clean value variable
    /// </summary>
    $value = CleanString($arrayDatasLoaded[4][0]);
    $sqlRequests = [
        'selectRequest' => "SELECT `idLangue` FROM `langues` WHERE `langue` LIKE '$value';",
        'insertRequest' => "INSERT INTO `langues` (`langue`) VALUES ('$value');"
    ];

    $idLangueO = ReturnId('idLangue', $sqlRequests, $DBConnection);

    /// <summary>
    ///     define sql settings for translation language
    ///     clean value variable
    /// </summary>
    $value = CleanString($arrayDatasLoaded[4][2]);
    $sqlRequests = [
        'selectRequest' => "SELECT `idLangue` FROM `langues` WHERE `langue` LIKE '$value';",
        'insertRequest' => "INSERT INTO `langues` (`langue`) VALUES ('$value')"
    ];

    $idLangueT = ReturnId('idLangue', $sqlRequests, $DBConnection);

////////////////////////////////////////////////////////////////////////////
//                        Expressions + se_rapporte
////////////////////////////////////////////////////////////////////////////

    /// <summary>
    ///     browse the translation data for each line of the current csv file starting from index 6
    ///     write into database if necessear
    /// </summary>
    /// <parameters>
    ///     $arrayDatasExpressions : array with the original and translation expressions of a csv line
    /// </parameters>
    for ($j=6; $j<sizeof($arrayDatasLoaded); $j++) {
        $arrayDatasExpressions = $arrayDatasLoaded[$j];

        /// <summary>
        ///     search index of first '' value in $arrayDatasExpressions
        /// </summary>
        /// <parameter>
        ///     $splitIndex : numeric value of index in array
        /// </parameter>
        $splitIndex = array_search('', $arrayDatasExpressions);
        
        /// <summary>
        ///     split $arrayDatasExpressions from 0 index to splitIndex
        /// </summary>
        /// <parameters>
        ///     $expressionsOrigine : array with all origine expressions from a csv files line
        /// </parameters>
        $expressionsOrigine = array_slice($arrayDatasExpressions, 0, $splitIndex);

        /// <summary>
        ///     define parameters for ReturnArrayId function
        /// </summary>
        /// <parameters>
        ///     $arrayExpressions : current arrat expressions
        ///     $idLangue : current idLangue for $arrayExpressions
        /// </parameters>
        $arrayExpressions = $expressionsOrigine;
        $idLangue = $idLangueO;

        /// <summary>
        ///     returns an array with the ids of the origine expressions to write in the correspondance table
        /// </summary>
        /// <parameters>
        ///     $idContexte : current id context
        /// </parameters>
        $arrayIdExpressionO = ReturnArrayId($arrayExpressions, $idLangue, $idContexte, $DBConnection);

        /// <summary>
        ///     split $arrayDatasExpressions from splitIndex+1 index (to eliminate '' value)
        ///     to the end of $arrayDatasExpressions
        /// </summary>
        /// <parameters>
        ///     $expressionsOrigine : array with all translation expressions from a csv files line
        /// </parameters>
        $expressionsTraduite = array_slice($arrayDatasExpressions, $splitIndex+1);
        
        /// <summary>
        ///     define parameters for ReturnArrayId function
        /// </summary>
        /// <parameters>
        ///     $arrayExpressions : current arrat expressions
        ///     $idLangue : current idLangue for $arrayExpressions
        /// </parameters>
        $arrayExpressions = $expressionsTraduite;
        $idLangue = $idLangueT;
        
        /// <summary>
        ///     returns an array with the ids of the translation expressions to write in the correspondance table
        /// </summary>
        /// <parameters>
        ///     $idContexte : current id context
        /// </parameters>
        $arrayIdExpressionT = ReturnArrayId($arrayExpressions, $idLangue, $idContexte, $DBConnection);

        ////////////////////////////////////////////////////////////////////////////
        //                        Correspondance
        ////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     checks if there is an image id associated with an origin id in the correspondance table, stock it if yes
        ///     associate each origin id with all translation id
        ///     check every association in database correspondance table
        ///     write in it if not existing
        /// </summary>
        /// <parameter>
        ///     $idImage : image link id
        ///     $idOrigine : value from origine expression array
        ///     $idTraduction : value from traduction expression array
        /// </parameter>
        /// <return>
        ///
        /// </return>
        foreach($arrayIdExpressionO as $idOrigine){
            $selectIdImageRequest = "   SELECT idImage FROM `correspondance` 
                                        INNER JOIN expressions expressionsO
                                                ON correspondance.idOrigine = expressionsO.idExpression
                                            LEFT JOIN se_rapporte se_rapporteO
                                                    ON expressionsO.idExpression = se_rapporteO.idExpression
                                        INNER JOIN expressions expressionsT
                                                ON correspondance.idTraduction = expressionsT.idExpression
                                            LEFT JOIN se_rapporte se_rapporteT
                                                    ON expressionsT.idExpression = se_rapporteT.idExpression
                                        WHERE idOrigine = $idOrigine AND (se_rapporteO.idContexte = se_rapporteT.idContexte) AND se_rapporteO.idContexte = $idContexte;";
            $resultIdImage = $DBConnection->query($selectIdImageRequest);
            $arrayIdImage = $resultIdImage->fetchall(PDO::FETCH_ASSOC);

            /// <summary>
            ///     declare and initialize id imae variable if value existe in database
            /// </summary>
            /// <return>
            ///     id image value
            /// </return>
            if(sizeof($arrayIdImage)>0){
                $idImage = $arrayIdImage[0]['idImage'];
            }

            foreach($arrayIdExpressionT as $idTraduction){

                /// <summary>
                ///     define insert sql request
                /// </summary>
                $insertCorrespondance = "INSERT INTO `correspondance` (`idOrigine`, `idTraduction`) VALUES ($idOrigine, $idTraduction);";

                /// <summary>
                ///     modify insert sql request if an id image value existe
                /// </summary>
                /// <return>
                ///     new sql insert sql request with id image
                /// </return>
                if(isset($idImage)){
                    $insertCorrespondance = "INSERT INTO `correspondance` (`idOrigine`, `idTraduction`, `idImage`) VALUES ($idOrigine, $idTraduction, $idImage);";
                }

                $selectCorrespondanceRequest = "SELECT `idOrigine`, `idTraduction`
                                                FROM `correspondance` 
                                                WHERE (`idOrigine` = '$idOrigine' AND `idTraduction` = $idTraduction);";
                $resultCorrespondance = $DBConnection->query($selectCorrespondanceRequest);
                $arrayCorrespondance = $resultCorrespondance->fetchall(PDO::FETCH_ASSOC);

                /// <return>
                ///     insert correspondance values if non existing in database
                /// </return>
                if(sizeof($arrayCorrespondance)==0){
                    $DBConnection->query($insertCorrespondance);
                }
            }
        }
    }

////////////////////////////////////////////////////////////////////////////
//                        End of script
////////////////////////////////////////////////////////////////////////////
    unset($arrayDatasLoaded);
    /// log
    echo "<p><b>closing file : ".$fileToLoad."</b></p>";
    fclose($fileLoaded);
}

////////////////////////////////////////////////////////////////////////////
//                        Functions
////////////////////////////////////////////////////////////////////////////

/// <summary>
///     filter files with csv extension
/// </summary>
/// <return>
///     array with files names
/// </return>
function FilterCsvFiles($files){
    $arrayCsvFilesNames = array_filter($files, function ($file){
        return pathinfo($file, PATHINFO_EXTENSION) === 'csv';
    });
    return ($arrayCsvFilesNames);
}

/// <summary>
///     search id on database or in array
///     insert value in database if necessary
/// </summary>
/// <parameters>
///     $column : sql column
///     $parameter : sql parameter
///     $value : sql value
///     $sqlRequests : array with select and insert sql requests used in function
///     $DBConnection : PDO connection
/// </parameters>
/// <return>
///     $id : database id of the value passed in function parameter
/// </return>
function ReturnId($column, $sqlRequests, $DBConnection){
    $selectRequest = $sqlRequests['selectRequest'];
    $insertRequest = $sqlRequests['insertRequest'];
    $result = $DBConnection->query($selectRequest);
    $selectResult = $result->fetchall(PDO::FETCH_ASSOC);
    if(sizeof($selectResult)==0){
        $DBConnection->query($insertRequest);
        $id = $DBConnection->lastInsertId();
        /// log
    }else{
        $id  = $selectResult[0][$column];
        /// log
    }
    return $id;
}

/// <summary>
///     delete space at the begininnig and the end of the string to eliminate potential duplicate
///     replace ' caracter by ` in order to insert string into database 
/// </summary>
/// <parameters>
///     $string : the string to clean
/// </parameters>
/// <return>
///     $cleanedString : a cleaned string
/// </return>
function CleanString($string){
    $cleanedString = trim($string);
    $cleanedString = str_replace("'", "`", $cleanedString);
    return $cleanedString;
}

/// <summary>
///     returns an array with the ids of the expressions to write in the correspondance table
///     insert expression in database if necessary
/// </summary>
/// <parameters>
///     $arrayExpressions : array with the expressions of a csv line either in origin or in translation
/// </parameters>
/// <return>
///     $arrayIdExpression : array with the database id of the different expressions from $arrayExpressions
/// </return>
function ReturnArrayId($arrayExpressions, $idLangue, $idContexte, $DBConnection){
    /// <summary>
    ///     declaration of the idExpression array
    /// </summary>
    $arrayIdExpression = [];

    foreach($arrayExpressions as $expression){
        $value = CleanString($expression);

        /// <summary>
        ///     go to next expression if current expression is empty
        /// </summary>
        if($value==''){continue;}

        /// <summary>
        ///     define expressions sql settings for ReturnId function
        /// </summary>
        $sqlRequests = [
            'selectRequest' => "SELECT `idExpression` FROM `expressions` WHERE (`expression` LIKE '$value' AND `idLangue` = $idLangue);",
            'insertRequest' => "INSERT INTO `expressions` (`expression`, `idLangue`) VALUES ('$value', $idLangue);"
        ];

        $idExpression = ReturnId('idExpression', $sqlRequests, $DBConnection);

        /// <summary>
        ///     define sql settings for se_rapporte table
        /// </summary>
        $selectSe_rapporte = "SELECT `idExpression`, `idContexte`FROM `se_rapporte` WHERE (`idExpression`= $idExpression AND `idContexte` = $idContexte);";
        $insertSe_rapporte = "INSERT INTO `se_rapporte` (`idExpression`,`idContexte`) VALUES ($idExpression, $idContexte);";

        /// <summary>
        ///     search already existing se_rapporte value on database
        ///     value associating an expression to a context
        ///     insert it if not
        /// </summary>
        $result = $DBConnection->query($selectSe_rapporte);
        $selectResult = $result->fetchall(PDO::FETCH_ASSOC);
        if(sizeof($selectResult)==0){
            $DBConnection->query($insertSe_rapporte);
            /// log
        }else{
            /// log
        }

        /// <summary>
        ///     add id expression into array
        /// </summary>
        array_push($arrayIdExpression, $idExpression);
    }
    return $arrayIdExpression;
}
?>